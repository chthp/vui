/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * Tab选项卡<br>
 * <pre>
tab1 = new VUI.Tab({
	renderId:'tab1'
	,items:[
	    // contentId值tab内容ID
		{title:'英文',contentId:'tab1-cont'}
		,{title:'中文',contentId:'tab2-cont',closable:true}
		,{title:'无法关闭',content:'无法关闭',closable:true}
	]
});
 * </pre>
 * @class VUI.Tab
 * @extends VUI.Component
 */
VUI.Class('Tab',{
	OPTS:{
		/**
		 * @cfg {Number} width 设置宽
		 */
		width:'auto'
		/**
		 * @cfg {Number} height 设置高
		 */
		,height:'auto'
		/**
		 * @cfg {Boolean} border 是否有边框线
		 */
		,border:true
		/**
		 * @cfg {Array} items
		 */
		,items:[]
		,loadFilter:null
	}
	// ----- 事件注释 -----
	
	/**
	 * @event onBeforeClose 在选项卡面板关闭的时候触发，返回false取消关闭操作。
	 * @param {Object} e e.item 选项卡数据,e.index 选项卡索引
	 */
	 
	/**
	 * @event onClose 在选项卡面板关闭后触发。
	 * @param {Object} e e.item 选项卡数据,e.index 选项卡索引
	 */
	 
	/**
	 * @event onSelect 用户选择一个选项卡面板的时候触发。
	 * @param {Object} e e.item 选项卡数据,e.index 选项卡索引
	 */
	
	/**
	 * 构造函数
	 * @constructor
	 * @ignore
	 */
	,init:function(opts) {
		this._super(opts);
	}
	,controller:function(data) {
		this.formatItems(this.tabs());
	}
	,tabs:function() {
		return this.opt('items');
	}
	/**
	 * 添加一个新选项卡
	 * @param {Object} item
	 */
	,add:function(item) {
		var items = this.opt('items');
		items.push(item);
		
		var index = items.length - 1;
		this.setNextDo(function(){
			if(item.selected) {
				this.select(index);
			}
			this.fire('Add',{item:item,index:index});
		});
	}
	/**
	 * 设置选项卡
	 * @param {Array} items
	 */
	,setItems:function(items) {
		this.formatItems(items);
		this.set('items',items);
	}
	/**
	 * 关闭一个选项卡面板
	 * @param {String|Number} titleOrIndex 可以是选项卡面板的标题或者索引
	 */
	,close:function(titleOrIndex) {
		this.view.close(titleOrIndex);
	}
	/**
	 * 选择一个选项卡面板
	 * @param {String|Number} titleOrIndex 可以是选项卡面板的标题或者索引
	 */
	,select:function(titleOrIndex) {
		this.view.select(titleOrIndex);
	}
	/**
	 * 返回一个选项卡面板
	 * @param {String|Number} titleOrIndex 可以是选项卡面板的标题或者索引
	 * @return item
	 */
	,getTab:function(titleOrIndex) {
		return this.view.getItem(titleOrIndex);
	}
	/**
	 * 返回选中的index
	 * @return 索引
	 */
	,getSelectedIndex:function() {
		var item = this.getSelected();
		return this.getTabIndex(item);
	}
	/**
	 * 返回索引
	 * @param {Object} item 选项卡item
	 */
	,getTabIndex:function(item) {
		return this.view.getTabIndex(item);
	}
	/**
	 * 返回选中的数据
	 * @return 返回item
	 */
	,getSelected:function() {
		return this.view.getSelected();
	}
	/**
	 * 判断一个选项卡面板是否存在
	 * @param {String|Number} titleOrIndex 可以是选项卡面板的标题或者索引
	 * @return {Boolean}
	 */
	,exists:function(titleOrIndex) {
		return this.view.exists(titleOrIndex);
	}
	/**
	 * 隐藏一个标签
	 * @param {String|Number} titleOrIndex 可以是选项卡面板的标题或者索引
	 * @return {Boolean}
	 */
	,hideItem:function(titleOrIndex) {
		this.view.hideItem(titleOrIndex);
	}
	/**
	 * 显示一个标签
	 * @param {String|Number} titleOrIndex 可以是选项卡面板的标题或者索引
	 * @return {Boolean}
	 */
	,showItem:function(titleOrIndex) {
		this.view.showItem(titleOrIndex);
	}
	/**
	 * 启用一个选项卡面板
	 * @param {String|Number} titleOrIndex 可以是选项卡面板的标题或者索引
	 */
	,enableTab:function(titleOrIndex) {
		this.view.enableTab(titleOrIndex);
	}
	/**
	 * 禁用一个选项卡面板
	 * @param {String|Number} titleOrIndex 可以是选项卡面板的标题或者索引
	 */
	,disableTab:function(titleOrIndex) {
		this.view.disableTab(titleOrIndex);
	}
	/**
	 * 更改选项卡标题
	 * @param {String|Number} titleOrIndex 可以是选项卡面板的标题或者索引
	 * @param {String} newTitle 新标题
	 */
	,setTitle:function(titleOrIndex,newTitle) {
		var that = this;
		var item = that.getTab(titleOrIndex);
		item.title = newTitle;
	}
	/**
	 * @ignore
	 */
	,formatItems:function(items) {
		if(items.length == 0) {
			return [];
		}
		
		this.runOptFun('loadFilter',items);
		
		var hasSelected = false;
		
		for(var i=0, len=items.length;i<len; i++) {
			var item = items[i];
			
			if(item.selected) {
				hasSelected = true;
				break;
			}
		}
		
		if(!hasSelected) {
			items[0].selected = true;
			this.startItem = items[0];
		}
		
		return items;
		
	}
	/**
	 * @ignore
	 */
	,getViewClass:function() {
		return VUI.TabView;
	}
},VUI.Component);

})();