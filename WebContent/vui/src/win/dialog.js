/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * Dialog对话框<br>
 * <pre>
win = new VUI.Dialog({
	contentId:'win'
	,title:'Dialog标题'
	,width:500
	,onOk:function() {
		alert('ok')
	}
});
 </pre>
 * @class VUI.Dialog
 * @extends VUI.Win
 */
VUI.Class('Dialog',{
	OPTS:{
		/**
		 * @cfg {Boolean} maximizable
		 */
		maximizable:false
		/**
		 * @cfg {Boolean} minimizable
		 */
		,minimizable:false
		/**
		 * @cfg {Boolean} collapsible
		 */
		,collapsible:false
		/**
		 * @cfg {Boolean} noCancelBtn 如果为true没有取消按钮
		 */
		,noCancelBtn:false
		/**
		 * @cfg {String} okText 确定按钮文本
		 */
		,okText:'确定'
		/**
		 * @cfg {String} cancelText 取消按钮文本
		 */
		,cancelText:'取消'
		/**
		 * @cfg {Function} onOk 点确定触发的方法
		 */
		,onOk:function() {
			
		}
		/**
		 * @cfg {Function} onCancel 点取消触发的方法,默认关闭窗口
		 */
		,onCancel:function() {
			this.hide();
		}
		,buttons:null
	}
	/**
	 * 构造函数
	 * @constructor
	 * @ignore
	 */
	,init:function(opts) {
		this._super(opts);
		
		this.buildDefButtons();
	}
	/**
	 * @ignore
	 */
	,getViewClass:function() {
		return VUI.DialogView;
	}
	/**
	 * @ignore
	 */
	,buildDefButtons:function() {
		var that = this;
		var buttons = this.opt('buttons');
		
		if(!buttons) {
			buttons = [
			 	{text:this.opt('okText'),handler:function(){
			 		var onOk = that.opt('onOk');
			 		onOk && onOk.call(that);
			 	}}
			]
			
			if(!this.opt('noCancelBtn')) {
				var cancelBtn = {text:this.opt('cancelText'),handler:function(){
			 		var onCancel = that.opt('onCancel');
			 		if(onCancel) {
			 			onCancel.call(that);
			 		}else{
			 			that.hide();
			 		}
			 	}};
				buttons.push(cancelBtn);
			}
			
			this.opt('buttons',buttons);
		}
	}
	
},VUI.Win);

})();